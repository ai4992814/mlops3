import logging
import os

file_log = logging.FileHandler(os.path.basename(__file__) + '.log')
console_out = logging.StreamHandler()

logging.basicConfig(handlers=(file_log, console_out), \
                    format="{asctime} [{levelname:8}] {process} {thread} {module}: {message}", \
                    style="{")
logging.getLogger('unicorn-log').setLevel(logging.ERROR)


def choose_project(name: str) -> int:
    """Choose project

    Args:
        name (str): Name project

    Returns:
        int: Length name project
    """

    logging.error(name)
    return len(name)

if __name__ == "__main__":
    choose_project('Choose a project topic')
