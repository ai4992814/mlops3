FROM continuumio/miniconda3

WORKDIR /app

COPY pyproject.toml \
     pdm.lock README.md \
     .pre-commit-config.yaml \
     notebooks ./

RUN conda install --yes -c  conda-forge pdm

RUN pdm install -v

RUN echo "source $(pdm info --packages)/bin/activate" >> ~/.bashrc

ENV PATH="/opt/conda/bin:/root/.local/bin:${PATH}"

CMD ["/bin/bash"]